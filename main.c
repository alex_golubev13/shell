#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <limits.h>
#include <fcntl.h>
#include "list.h"
#include "tree.h"

#define SIZE 10

jmp_buf start_inp;//Приглашение на начало ввода
jmp_buf begin; //Точка начала диалога с пользователем в L-графе

const char correct_symbols[] = {'1','2','3','4','5','6','7','8','9','0',
                                'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                                'A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                '\t','\n',' ','$','_','/','.','-', '"', '\\', ':',
                                '|','&',';','<','>','(',')','\0'};//Корректные символы

//************ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ************** *****************
void str_add_symbol(buffer *buf, char x){//Добавление символа к слову
    if ((*buf).curbuf > (*buf).sizebuf-1) { //Увеличиваем буфер на SIZE, если не хватает места
        (*buf).sizebuf+=SIZE;
        (*buf).line = realloc((*buf).line, (size_t )((*buf).sizebuf));
    }
    (*buf).line[(*buf).curbuf++] = x;
}

void init_words(array *words){//Инициализация массива слов
    (*words).list = NULL;
    (*words).sizelist = 0;
    (*words).curlist = 0;
}

void init_buf(buffer *buf){//Инициализация буфера, так же вызывается в обработке L-графа, чтобы обнулить буфер
    (*buf).line = NULL;
    (*buf).sizebuf = 0;
    (*buf).curbuf = 0;
}

int corectness(char x){//Проверка корректности символа
    int i;
    for(i = 0; correct_symbols[i] != '\0'; i++){
        if (x == correct_symbols[i])
            break;
    }
    if (correct_symbols[i] == '\0')
        return 1;//Символ некорректный
    return 0;//Символ корректный
}
//*******************************************************************

void check_mistake(array *w){ //Процедура вызывается после добавления слово или создания, где используются realloc, чтобы отлавить ошибки
    if (errno){
        fprintf(stderr,"Crash\n");
        delete_list(w);
        longjmp(begin, 1);
    }
}

void copy_string(char *s1, char *s2){
    while(*s2 != '\0')
        *s1++ = *s2++;
    *s1 = *s2;
}

void add_shell_command(cmd_inf *t, cmd_inf *sub, char **a, int var, char *inf, int output_mode, char *outf, int condition, cmd_inf *pointer){//Добавление команды в дерево
    if (*t != NULL){
        if (var == 0)
            add_shell_command(&(*pointer)->next, sub, a, var, inf, output_mode, outf, condition, pointer);
        else if ((*t)->pipe != NULL)
            add_shell_command(&(*t)->pipe, sub, a, var, inf, output_mode, outf, condition, pointer);
        else if ((*t)->next != NULL)
            add_shell_command(&(*t)->next, sub, a, var, inf, output_mode, outf, condition, pointer);
        else
            add_shell_command(&(*t)->pipe, sub, a, var, inf, output_mode, outf, condition, pointer);
    }
    else {
        *t = (cmd_inf) malloc(sizeof(structure));
        int l = lenwords(a);
        (*t)->argv = (char **) malloc((l+1) * sizeof(a));//Выделяем память для массива слов
        int i;
        int numb;
        for (i = 0; a[i] != NULL; i++){//Для каждого слова выделяем память
            numb = strlen(a[i]);
            *((*t)->argv + i) = (char *) malloc(numb * sizeof(a[i]));
            copy_string((*t)->argv[i], a[i]);
        }
        (*t)->argv[i] = NULL;
        (*t)->infile = (char *) malloc(PATH_MAX);
        copy_string((*t)->infile, inf);
        if (output_mode == 2)
            (*t)->mode = 2;
        else if (output_mode == 1)
            (*t)->mode = 1;
        else
            (*t)->mode = 0;
        (*t)->outfile = (char *) malloc(PATH_MAX);
        copy_string((*t)->outfile, outf);
        (*t)->backgrnd = 0;
        (*t)->psubcmd = *sub;
        (*t)->pipe = NULL;
        (*t)->next = NULL;
        (*t)->cond = condition;
        if (var == 0)
            *pointer = *t;
    }
}

void l_graph(array *shell_line){
    enum symbol {start, separator, one_sign, mb_two_signs, quotes, bufend, newline}
            v = start;//Создаем перечислимый тип, чтобы находить разделители
    buffer buf;
    char str[SIZE + 1];//Массив, куда считываются порционно данные функцией read
    int i = 0;//Указывает на очередной символ в str
    int cor;//Корректность символа
    int count;//Хранит кол-во считанных символов функцией read
    char tmp = ' ';//Вспомогательный символ для отслеживания двойных разделителей
    char cur = ' ';//Очередной считанный символ
    setjmp(begin); //Метка, чтобы в случае ошибки не выходить из программы, а пригласить заново ввести текст
    errno = 0; //Используется для отлавливания ошибок
    init_buf(&buf);
    init_words(shell_line);
    count = read(0, str, SIZE);
    if (count == 0)
        exit(0);
    *(str + count) = '\0';
    while (1) {
        if (ferror(stdin)) {
            fprintf(stderr,"Input stream error");
            check_mistake(shell_line);
        }
        else {
            switch (v) {
                case start:
                    cur = *(str + i);
                    tmp = cur;
                    if (cur == '\0')
                        v = bufend;
                    else{
                        cor = corectness(cur);
                        if (cor)//Если символ некорректный, то нужно очистить память и считать строку до \n, чтобы далее начать обрабатывать символы со следующей строки
                        {
                            fprintf(stderr,"Incorrect input\n");
                            align_list(shell_line);
                            delete_list(shell_line);
                            free(buf.line);
                            init_buf(&buf);
                            init_words(shell_line);
                            while((cur = *(str + (i++))) != '\n'){//Читаем пока не встретим символ новой строки
                                if (cur == '\0') {
                                    count = read(0, str, SIZE);
                                    *(str + count) = '\0';
                                    i=0;
                                }
                            }
                            v = start;
                        }
                        else if (cur == ' ' || cur == '\t')
                            v = separator;
                        else if (cur == '\n')
                            v = newline;
                        else if (cur == ';' || cur == '(' || cur == ')' || cur == '<')
                            v = one_sign;
                        else if (cur == '"')
                            v = quotes;
                        else if (cur == '>' || cur == '|' || cur == '&')
                            v = mb_two_signs;
                        else{
                            str_add_symbol(&buf, cur);
                            check_mistake(shell_line);
                            i++;
                        }
                    }
                    break;
                case separator:
                    if (buf.curbuf >= 1) {
                        append_node(shell_line, &buf);
                        check_mistake(shell_line);
                    }
                    init_buf(&buf);
                    i++;
                    v = start;
                    break;
                case newline:
                    if (buf.curbuf >= 1) {
                        append_node(shell_line, &buf);
                    }
                    align_list(shell_line);
                    check_mistake(shell_line);
                    goto ex;
                case one_sign:
                    if (buf.curbuf >= 1) {//Обработка пустого буфера
                        append_node(shell_line, &buf);
                        check_mistake(shell_line);
                    }
                    init_buf(&buf);
                    i++;
                    str_add_symbol(&buf, tmp);
                    append_node(shell_line, &buf);
                    check_mistake(shell_line);
                    init_buf(&buf);
                    v = start;
                    break;
                case quotes:
                    i++;
                    cur = *(str + i);
                    while (cur != '"'){
                        if (cur == '\0'){
                            if (feof(stdin))
                                goto ex;
                            count = read(0, str, SIZE);
                            *(str + count) = '\0';
                            i=0;
                            cur = *str;
                        }
                        else {
                            str_add_symbol(&buf, cur);
                            i++;
                            cur = *(str + i);
                        }
                    }
                    /*if (buf.curbuf >= 1) {
                        append_node(shell_line, &buf);
                        check_mistake(shell_line);
                    }
                    init_buf(&buf);*/
                    i++;
                    v = start;
                    break;
                case mb_two_signs:
                    i++;
                    cur = *(str + i);
                    if (cur == tmp){//Если один и тот же разделитель встритлся 2 раза подряд, то добавляем сначала буфер, потом сам разделитель
                        if  (buf.curbuf >= 1) {
                            append_node(shell_line, &buf);
                            check_mistake(shell_line);
                            init_buf(&buf);
                        }
                        str_add_symbol(&buf, cur);//Формируем буфер, состоящий из двойного разделителя
                        str_add_symbol(&buf, cur);
                        append_node(shell_line, &buf);
                        check_mistake(shell_line);
                        init_buf(&buf);
                        i++;
                        v = start;
                    }
                    else if (cur == '\0') {
                        v = bufend;
                    }
                    else {
                        i--;//Нужно откинуть счетчик i, чтобы корректно продолжать считывать символы
                        v = one_sign;
                    }
                    break;
                case bufend:
                    if (feof(stdin)) {
                        if (buf.curbuf >= 1)
                            append_node(shell_line, &buf);
                        align_list(shell_line);
                        check_mistake(shell_line);
                        goto ex;
                    }
                    count = read(0, str, SIZE);
                    *(str + count) = '\0';
                    i = 0;
                    if (tmp == '>' || tmp == '&' || tmp == '|') {
                        i--;
                        v = mb_two_signs;
                    }
                    else
                        v = start;
                    break;
            }
        }
    }
    ex:
    return;
}

int check_command(char *name){
    if (strcmp(name,"|") == 0 || strcmp(name, "||") == 0 || strcmp(name,"&") == 0 || strcmp(name,"&&") == 0 || strcmp(name, ">") == 0 || strcmp(name, ">>") == 0 || strcmp(name,"<") == 0 || strcmp(name,";") == 0 || strcmp(name,")") == 0)
        return 0;
    return 1;
}

void mistake_reaction(cmd_inf *shell, cmd_inf *subshell, array *shell_line){
    delete_list(shell_line);
    clear_tree(shell);
    clear_tree(subshell);
}


typedef struct Node *connect;

typedef struct Node{
    pid_t elem;
    connect next;
} node;

typedef connect list;

list pids = NULL;

void append_pid(list *p, pid_t s){ //Добавление pid в конец
    if (*p == NULL){
        *p = (list) malloc(sizeof(node));
        (*p)->elem = s;
        (*p)->next = NULL;
        return;
    }
    append_pid(&(*p)->next, s);
}

void delete_pid(list *p, pid_t del) { //Удаление pid
    if ((*p)->next != NULL) {
        if ((*p)->elem == del) {
            list q = *p; //Ссылаемся на узел, который нужно удалить
            (*p) = (*p)->next; //Обходим узел
            free(q); //Освобождаем пространство
        } else
            delete_pid(&(*p)->next, del);
    }
    else {
        if ((*p)->elem == del) {
            list q = *p; //Ссылаемся на узел, который нужно удалить
            (*p) = (*p)->next; //Обходим узел
            free(q); //Освобождаем пространство
        }
    }
}

void wait_pid_backgrnd(){ //Удаляем зомби фоновых процессов
    int status = 0;
    while (pids != NULL) {
        waitpid(pids->elem, &status, WNOHANG);
        list q = pids;
        pids = pids->next;
        if (WIFEXITED(status))
            delete_pid(&q, q->elem);
    }
}

int wait_pid_common(list *p){ //Ждем процессы, которые работают в обычном режиме
    int status;
    while (*p != NULL){
        waitpid((*p)->elem, &status, 0);
        list q = *p;
        (*p) = (*p)->next;
        delete_pid(&q, q->elem);
    }
    return status;
}

void delete_list_pids(list *p){ //Удаление всего списка в конце программы
    while (*p != NULL){
        list q = *p;
        (*p) = (*p)->next;
        free(q);
    }
}

int execute(cmd_inf t);

void exec_cmd(cmd_inf t) {
    int inf;
    int outf;
    int status;
    if (strcmp(t->infile, "0") != 0) {
        inf = open(t->infile, O_RDONLY, 0666);
        dup2(inf, 0);
    }
    if (strcmp(t->outfile, "0") != 0) {
        if (t->mode == 2)
            outf = open(t->outfile, O_APPEND | O_WRONLY | O_CREAT, 0666);
        else
            outf = open(t->outfile, O_WRONLY | O_CREAT | O_TRUNC, 0666);
        dup2(outf, 1);
    }
    if (t->backgrnd == 1) {
        signal(SIGINT, SIG_IGN); //Игнорируем сигнал Ctrl+C, если процесс фоновый
    }
    if (t->psubcmd != NULL) {
        status = execute(t->psubcmd);
        clear_tree(&t);
        if (status > 0) 
            exit(-1);
        else
            exit(0);
    
    }
    execvp(t->argv[0], t->argv);
    fprintf(stderr, "%s: Command not found\n", t->argv[0]);
    clear_tree(&t);
    exit(-1);
}


void exec_tree_conv(cmd_inf t, int *status) { //Выполнение конвейера
    pid_t pid;
    list pids1 = NULL;
    int fd[2];
    int fd_helper;//Вспомогательная переменная для реализации цепочки конвейеров
    int change;
    cmd_inf current = t;
    if (current->argv[0] != NULL && (strcmp(current->argv[0], "cd") == 0)) {
        change = chdir(current->argv[1]);
        if (change != 0)
            perror("No such file or directory");
        return;
    }
    if (current->pipe != NULL) {
        if (pipe(fd) < 0)
            exit(1);
        if ((pid = fork()) == 0) {
            dup2(fd[1], 1);
            close(fd[1]);
            close(fd[0]);
            exec_cmd(current);
            //exit(0); //Процесс subshell завершается
        }
        if (current->backgrnd == 1)
            append_pid(&pids, pid);
            //waitpid(pid, status, 0);
        else
            append_pid(&pids1, pid);
        fd_helper = fd[0];
        current = current->pipe;
        while (current->pipe != NULL) {
            close(fd[1]);
            pipe(fd);
            if ((pid = fork()) == 0) {
                dup2(fd_helper, 0); //Читаем из предыдущего канала
                close(fd_helper);
                dup2(fd[1], 1); //Пишем уже в следующий
                close(fd[1]);
                close(fd[0]);
                exec_cmd(current);
                //exit(0); //Процесс subshell завершается
            }
            if (current->backgrnd == 1)
                append_pid(&pids, pid);
                //waitpid(pid, status, 0);
            else
                append_pid(&pids1, pid); //pids1 для обычных процессов
            close(fd_helper);
            fd_helper = fd[0];
            current = current->pipe;
        }
        close(fd[1]);
        if ((pid = fork()) == 0) {
            dup2(fd_helper, 0);
            close(fd_helper);
            exec_cmd(current);
            //exit(0); //Процесс subshell завершается
        }
        if (current->backgrnd == 1)
            append_pid(&pids, pid);
            //waitpid(pid, status, 0);
        else
            append_pid(&pids1, pid);
        close(fd_helper);
        *status = wait_pid_common(&pids1);
    }
    else {
        if ((pid = fork()) == 0) {
            exec_cmd(current);
            //exit(0); //Процесс subshell завершается
        }
        if (current->backgrnd == 1)
            append_pid(&pids, pid);
        else
            waitpid(pid, status, 0);
    }
    wait_pid_backgrnd();
}

int execute(cmd_inf t){
    int status = 0; //Статус завершения конвейера
    while (t != NULL){
        exec_tree_conv(t, &status);
        if(t->cond == 1) { //Если &&
            if (!WIFEXITED(status) || WEXITSTATUS(status)) { //Условие, при котором не будет выполняться следующая команда
                while (t != NULL && t->cond == 1) //Пока идут &&, пропускаем команды
                    t = t->next;
            }
            if (t != NULL)
                t = t->next;
        }
        else if (t ->cond == 2) { //Если ||
            if (WIFEXITED(status) && !WEXITSTATUS(status)) { //Условие, при котором не будет выполняться следующая команда
                while (t != NULL && t->cond == 2) //Пока идут ||, пропускаем команды
                    t = t->next;
            }
            if (t != NULL)
                t = t->next;
        }
        else {
            t = t->next;
        }
    }
    delete_list_pids(&pids);
    return status;
}

void build_tree(cmd_inf *shell, array *shell_line, int *i, int *brackets, int *flag) {
    enum shtree {conv, next_pipe, backgrnd, st_sbshell, end_sbshell, in, out, cond}
            v = conv;
    cmd_inf sub_shell = NULL;// Указатель на структуру для sub shell'a
    cmd_inf pointer = NULL;// Указатель на начало очередного конвейера (для реализации &)
    int index;
    int condition;
    char **command;
    char inf[PATH_MAX];
    char outf[PATH_MAX];
    int len;
    int path;
    int mode;
    int out_flag = 0;
    char *env;
    index = 0;
    path = 0;
    mode = 0;
    condition = 0;
    strcpy(inf, "0");
    strcpy(outf, "0");
    clear_tree(shell);
    command = (char **) malloc((*shell_line).sizelist * sizeof((*shell_line).list));//Создаем массив слов
    while (((*shell_line).list[*i] != NULL) && (*flag != 1)) {
        switch (v) {
            case conv:
                if (strcmp((*shell_line).list[*i], ";") == 0 || strcmp((*shell_line).list[*i], "|") == 0)
                    v = next_pipe;
                else if (strcmp((*shell_line).list[*i], "&") == 0)
                    v = backgrnd;
                else if (strcmp((*shell_line).list[*i], "(") == 0)
                    v = st_sbshell;
                else if (strcmp((*shell_line).list[*i], ")") == 0)
                    v = end_sbshell;
                else if (strcmp((*shell_line).list[*i], ">") == 0 || strcmp((*shell_line).list[*i], ">>") == 0) {
                    out_flag++;
                    v = out;
                }
                else if (strcmp((*shell_line).list[*i], "<") == 0)
                    v = in;
                else if ((strcmp((*shell_line).list[*i], "&&") == 0) || (strcmp((*shell_line).list[*i], "||")) == 0)
                    v = cond;
                else {
                    if ((*shell_line).list[*i + 1] != NULL) {
                        if (strcmp((*shell_line).list[*i + 1], "(") ==
                            0) {//Если после слова идет скобка, то ловим ошибку
                            mistake_reaction(shell, &sub_shell, shell_line);
                            *flag = 1;
                            goto ex;
                        }
                    }
                    if (strcmp((*shell_line).list[*i],"$HOME") == 0) {
                        command[index] = (char *) malloc(PATH_MAX);
                        env = getenv("HOME");
                        strcpy(command[index], env);
                    }
                    else if (strcmp((*shell_line).list[*i],"$USER") == 0) {
                        command[index] = (char *) malloc(PATH_MAX);
                        env = getenv("USER");
                        strcpy(command[index], env);
                    }
                    else if (strcmp((*shell_line).list[*i],"$SHELL") == 0) {
                        command[index] = (char *) malloc(PATH_MAX);
                        env = getenv("SHELL");
                        strcpy(command[index], env);
                    }
                    else {
                        len = strlen((*shell_line).list[*i]);
                        command[index] = (char *) malloc(
                                len * sizeof((*shell_line).list[*i]));//Выделяем место под отдельное слово
                        strcpy(command[index], (*shell_line).list[*i]);
                    }
                    index++;
                    (*i)++;
                }
                break;
            case next_pipe:
                if (*i == 0 || out_flag > 1) {
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                    goto ex;
                }
                command[index] = NULL;//Последнее слово в массиве задаем NULL
                if (command[0] != NULL || sub_shell != NULL) {
                    add_shell_command(shell, &sub_shell, command, path, inf, mode, outf, condition, &pointer);
                    out_flag = 0;
                    mode = 0;
                    clear_command(&command);
                    sub_shell = NULL;
                }
                index = 0;
                command[index] = NULL;
                //Задаем параметры по умолчанию
                strcpy(inf, "0");
                strcpy(outf, "0");
                condition = 0;
                if (strcmp((*shell_line).list[*i], ";") ==
                    0) //Определяем, откуда вести следующую связь: от next или от pipe
                    path = 0;
                else
                    path = 1;
                (*i)++;
                if ((((*shell_line).list[*i] != NULL) && (!check_command((*shell_line).list[*i]))) || ((*shell_line).list[*i] == NULL)) { //Если после разделителя идет опять разделитель или конец ввода, ловим ошибку
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                    goto ex;
                }
                v = conv;
                break;
            case backgrnd:
                command[index] = NULL;
                if (*i == 0 || out_flag > 1) {
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                    goto ex;
                }
                if (command[0] != NULL || sub_shell != NULL) {
                    add_shell_command(shell, &sub_shell, command, path, inf, mode, outf, condition, &pointer);
                    out_flag = 0;
                    mode = 0;
                    clear_command(&command);
                    sub_shell = NULL;
                }
                add_background(pointer);
                index = 0;
                command[index] = NULL;
                path = 0;
                strcpy(inf, "0");
                strcpy(outf, "0");
                (*i)++;
                if ((*shell_line).list[*i] == NULL)
                    break;
                if (!check_command((*shell_line).list[*i])) {
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                    goto ex;
                }
                v = conv;
                break;
            case st_sbshell:
                (*i)++;//Переходим к следующему слову
                (*brackets)++;
                build_tree(&sub_shell, shell_line, i, brackets, flag);//Запускаем рекурсию
                if (*flag == 1) {
                    clear_tree(&sub_shell);
                    goto ex; //Выодим из процедуры: если до этого мы уже вызывали рекурсию, то опять окажемся здесь и снова выйдем
                }
                if ((((*shell_line).list[*i] != NULL) && (check_command((*shell_line).list[*i]))) || *i == 0) { //Если после закрывающей скобки идет не разделитель, ловим ошибку
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                    goto ex;
                }
                else if((*shell_line).list[*i] == NULL) {
                    command[index] = NULL;
                    index++;
                }
                else
                    v = conv;
                break;
            case end_sbshell:
                (*brackets)--;
                if (index == 0){
                    command[index] = NULL;
                    index++;
                }
                if (*brackets < 0 || strcmp((*shell_line).list[*i-1], "(") == 0) {
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                }
                goto ex;
            case in:
                if ((((*shell_line).list[*i+1] == NULL) || (!check_command((*shell_line).list[*i+1]))) || *i == 0) { //Если после < идет разделитель, ловим ошибку
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                    goto ex;
                }
                strcpy(inf, (*shell_line).list[++*i]);
                (*i)++;
                if ((sub_shell != NULL) && (*shell_line).list[*i] == NULL) {
                    command[index] = NULL;
                    index++;
                }
                v = conv;
                break;
            case out:
                if((((*shell_line).list[*i+1] == NULL) || (!check_command((*shell_line).list[*i+1]))) || *i == 0) { //Если после > идет разделитель, ловим ошибку
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                    goto ex;
                }
                if (strcmp((*shell_line).list[*i], ">") == 0)
                    mode = 1;
                else
                    mode = 2;
                strcpy(outf, (*shell_line).list[++*i]);
                (*i)++;
                if ((sub_shell != NULL) && (*shell_line).list[*i] == NULL) {
                    command[index] = NULL;
                    index++;
                }
                v = conv;
                break;
            case cond:
                if (*i == 0 || out_flag > 1) {
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                    goto ex;
                }
                if (strcmp((*shell_line).list[*i], "&&") == 0)
                    condition = 1;
                else
                    condition = 2;
                command[index] = NULL;
                if (command[0] != NULL || sub_shell != NULL) {
                    add_shell_command(shell, &sub_shell, command, path, inf, mode, outf, condition, &pointer);
                    out_flag = 0;
                    mode = 0;
                    clear_command(&command);
                    sub_shell = NULL;
                }
                else if (command[0] != NULL) {
                    add_shell_command(shell, &sub_shell, command, path, inf, mode, outf, condition, &pointer);
                    out_flag = 0;
                    mode = 0;
                    clear_command(&command);
                    sub_shell = NULL;
                }
                add_mode(pointer, condition);
                index = 0;
                command[index] = NULL;
                strcpy(inf, "0");
                strcpy(outf, "0");
                condition = 0;
                path = 0;
                (*i)++;
                if (((*shell_line).list[*i] != NULL) && (!check_command((*shell_line).list[*i]))) {
                    mistake_reaction(shell, &sub_shell, shell_line);
                    *flag = 1;
                    goto ex;
                }
                v = conv;
                break;
        }
    }
    ex:
    command[index] = NULL;
    if (*flag == 0) {
        if ((((*shell_line).list[*i] == NULL) && (*brackets > 0)) || out_flag > 1) {
            mistake_reaction(shell, &sub_shell, shell_line);
            *flag = 1;
        }
        else if (index >= 1)
            add_shell_command(shell, &sub_shell, command, path, inf, mode, outf, condition, &pointer);
        clear_command(&command);
        free(command);
        command = NULL;
        (*i)++;
    }
    else{
        clear_command(&command);
        free(command);
        command = NULL;
    }
}

void sighndlr(int s){ //Обработчик сигнала для главного процесса
    wait_pid_common(&pids);
    siglongjmp(start_inp, 1);
}

int main() {
    cmd_inf shell = NULL;
    array line;
    signal(SIGINT, sighndlr);
    sigsetjmp(start_inp, 1);
    printf("\n");
    clear_tree(&shell);
    int i;
    int brackets;//Кол-во скобок
    int flag;//Если flag = 1, то нарушен баланс скобок, нужно выйти из всех рекурсий и вывести сообщение об ошибке
    char inf[PATH_MAX];
    char outf[PATH_MAX];
    strcpy(inf, "0");
    strcpy(outf, "0");
    while (1) {
        printf("$\n");
        i = 0;
        brackets = 0;
        flag = 0;
        l_graph(&line);
        //print(line);
        if (line.list != NULL) {
            if (strcmp(line.list[0], "exit") == 0)
                break;
            build_tree(&shell, &line, &i, &brackets, &flag);
            if(flag == 1){
                delete_list(&line);
                clear_tree(&shell);
                printf("WRONG INPUT\n");
                sigsetjmp(start_inp, 1);
            }
        }
        //print_str(shell);
        //printf("\n");
        delete_list(&line);
        execute(shell);
        clear_tree(&shell);
    }
    delete_list(&line);
    return 0;
}
