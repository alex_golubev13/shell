shell: main.c list.o tree.o
	gcc -g -Wall main.c list.o tree.o -o shell
list.o: list.c list.h
	gcc -g -Wall -c list.c -o list.o
tree.o: tree.c tree.h list.h
	gcc -g -Wall -c tree.c -o tree.o
