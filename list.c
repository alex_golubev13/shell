#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "list.h"

#define SIZE 10

int lenwords(char **s){//Подсчет кол-во слов в массиве
    if (s == NULL)
        return 0;
    int len = 0;
    while (*(s++) != NULL)
        len++;
    return len;
}

void append_node(array* words, buffer* buf){//Добавление слова в массив слов
    if ((*buf).curbuf>(*buf).sizebuf-1) {
        (*buf).line = realloc((*buf).line, (size_t) ((*buf).sizebuf + 1));//Увеличиваем место на один, чтобы положить символ \0 в конец
    }
    (*buf).line[(*buf).curbuf++]='\0';
    (*buf).sizebuf = (*buf).curbuf;
    (*buf).line=realloc((*buf).line, (size_t )((*buf).sizebuf));//Выравниваем место, отведенное на слово
    if ((*words).curlist > (*words).sizelist-1) { //Увеличиваем кол-во элементов в массиве, если недостаточно
        (*words).sizelist+= SIZE;
        (*words).list = realloc((*words).list,(*words).sizelist * sizeof(*(*words).list));
    }
    (*words).list[(*words).curlist++]=(*buf).line;
}

void delete_list(array *p){ //Удаление всего списка в конце программы
    if ((*p).list==NULL) return;
    int i;
    for (i=0; (*p).list[i]!=NULL; i++) //Сначала удаляем все слова, затем сам массив
        free((*p).list[i]);
    free((*p).list);
    (*p).list = NULL;
}

void print(array p)//Печать всех слов
{
    if (p.list == NULL) {
        printf("\nNULL\n");
        return;
    }
    else {
        while (*(p.list) != NULL) {
            printf("%s\n", *(p.list));
            p.list = p.list + 1;
        }
        printf("NULL\n");
    }
}

void align_list(array* words){
    if ((*words).list==NULL) return;
    if ((*words).curlist>(*words).sizelist-1) {
        (*words).list = realloc((*words).list, ((*words).sizelist + 1)*sizeof(*(*words).list));//Добавление одного элемента массива, чтобы положить в конце NULL.
    }
    (*words).list[(*words).curlist]=NULL;
    (*words).sizelist = (*words).curlist+1;
    (*words).list = realloc((*words).list, (*words).sizelist*sizeof(*(*words).list));//Выравнивание памяти по размеру
}