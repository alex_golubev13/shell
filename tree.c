#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <errno.h>
#include <limits.h>
#include "list.h"
#include "tree.h"

void print_str(cmd_inf t){
    int i = 0;
    printf("\n");
    if (t != NULL){
        printf("Address: %lu\n", (long) t);
        printf("Arguments: ");
        while (t->argv[i] != NULL){
            printf("%s ",t->argv[i++]);
        }
        printf("\n");
        printf("Input File: %s\n",t->infile);
        printf("Output Mode: %d\n",t->mode);
        printf("Output File: %s\n",t->outfile);
        printf("Background Mode: %d\n",t->backgrnd);
        //*******ОТЛАДОЧНАЯ ПЕЧАТЬ*****************
        if(t->psubcmd != NULL)
            printf("Subshell: %lu\n", (long) t->psubcmd);
        else
            printf("Subshell: NULL\n");
        if(t->pipe != NULL)
            printf("Pipe: %lu\n", (long) t->pipe);
        else
            printf("Pipe: NULL\n");
        if(t->next != NULL)
            printf("Next: %lu\n", (long) t->next);
        else
            printf("Next: NULL\n");
        printf("Condition: ");
        switch(t->cond){
            case 1:
                printf("&&\n");
                break;
            case 2:
                printf("||\n");
                break;
            default:
                printf("none\n");
                break;
        }
        //*****************************************
        if(t->psubcmd != NULL)
            print_str(t->psubcmd);
        if(t->pipe != NULL)
            print_str(t->pipe);
        if(t->next != NULL)
            print_str(t->next);
    }
}

void clear_command(char ***w){
    if (*w == NULL)
        return;
    int i;
    for (i = 0; (*w)[i] != NULL; i++)
        free((*w)[i]);
}

void clear_tree_helper(cmd_inf *t){
    if (*t != NULL){
        cmd_inf q = *t;
        clear_command(&q->argv);
        free(q->argv);
        q->argv = NULL;
        free(q->infile);
        q->infile = NULL;
        free(q->outfile);
        q->outfile = NULL;
        clear_tree_helper(&(*t)->psubcmd);
        clear_tree_helper(&(*t)->pipe);
        clear_tree_helper(&(*t)->next);
        free(q);
    }
    return;
}

void clear_tree(cmd_inf *t){
    clear_tree_helper(t);
    *t = NULL;
    return;
}

void add_background(cmd_inf p){
    while(p->pipe != NULL){
        p->backgrnd = 1;
        p = p->pipe;
    }
    p->backgrnd = 1;
}

void add_mode(cmd_inf p, int mode){
    while(p->pipe != NULL){
        p->cond = mode;
        p = p->pipe;
    }
    p->cond = mode;
}