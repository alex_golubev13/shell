#pragma once
typedef struct Cmd_inf *cmd_inf;

typedef struct Cmd_inf {
    char **argv; // список из имени команды и аргументов
    char *infile; // переназначенный файл стандартного ввода
    int mode; // 2, если >>; 1, если > и 0, если вывод стандартный
    char *outfile; // переназначенный файл стандартного вывода
    int backgrnd; // =1, если команда подлежит выполнению в фоновом режиме
    cmd_inf psubcmd; // команды для запуска в дочернем shell
    cmd_inf pipe; // следующая команда после “|”
    cmd_inf next; // следующая после “;” (или после “&”)
    int cond; //0 - none, 1 - &&, 2 - ||
} structure;

void print_str(cmd_inf t);
void clear_command(char ***w);
void clear_tree_helper(cmd_inf *t);
void clear_tree(cmd_inf *t);
void add_background(cmd_inf p);
void add_mode(cmd_inf p, int m);
