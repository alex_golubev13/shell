#pragma once
typedef struct Array{
    char **list;
    int sizelist;
    int curlist;
} array;

typedef struct Buffer{
    char *line;
    int sizebuf;
    int curbuf;
} buffer;

int lenwords(char **s);
void append_node(array *l, buffer *b);
void delete_list(array *l);
void print(array l);
void align_list(array *l);